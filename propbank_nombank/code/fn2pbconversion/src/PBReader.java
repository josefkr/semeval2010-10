import java.io.File;
import java.util.Hashtable;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class PBReader
{
	Hashtable<String, Hashtable<String, PBArgmap>> h_predicate;
	
	/** pnb = "pb"|"nb" */
	public PBReader(String filename, String pnb)
	{
		init(filename, pnb);
	}
	
	private void init(String filename, String pnb)
	{
		DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();
		String                predicate = (pnb.equals("pb")) ? PBLib.PREDICATE : PBLib.NOUN;
		h_predicate = new Hashtable<String, Hashtable<String, PBArgmap>>();
		
		try
		{
			DocumentBuilder dBuilder    = dFactory.newDocumentBuilder();
			Document        doc         = dBuilder.parse(new File(filename));
			NodeList        lsPredicate = doc.getElementsByTagName(predicate);
			
			for (int i=0; i<lsPredicate.getLength(); i++)
			{
				Element  ePredicate = (Element)lsPredicate.item(i);
				String   lemma      = ePredicate.getAttribute(PBLib.LEMMA).trim();
				NodeList lsArgmap   = ePredicate.getElementsByTagName(PBLib.ARGMAP);
				
				Hashtable<String, PBArgmap> hArgmap = new Hashtable<String, PBArgmap>();
				
				for (int j=0; j<lsArgmap.getLength(); j++)
				{
					Element eArgmap = (Element)lsArgmap.item(j);
					String  fnFrame = eArgmap.getAttribute(PBLib.FN_FRAME).trim();
					
					if (hArgmap.containsKey(fnFrame))
						System.err.println("- Warning: duplicated fn-frame, lemma="+lemma+", fn-frame="+fnFrame);
					else
						hArgmap.put(fnFrame, new PBArgmap(eArgmap, pnb));
				}
				
				h_predicate.put(lemma, hArgmap);
			}
		}
		catch (Exception e) {System.err.println(e);}
	}
	
	public PBArgmap getArgmap(String lemma, String fnFrame)
	{
		return h_predicate.containsKey(lemma) ? h_predicate.get(lemma).get(fnFrame) : null;
	}
}
