 Tue Nov  4 11:52:21 CET 2014

###########################
### clear_annotation.pl ###
###########################

Use this to automatically remove annotation data from an xml-file.

Run like this:
> perl clear_annotation.pl "path/to/file.xml"

This script will generate two new files *in the folder of the original xml-file*

File 1: $filename + "_fulltask.xml", containing no annotation data except the frames' names and invoking word(s)
File 2: $filename + "_no-instantiation.xml", containing all annotation data except no-instantiations

Coreference-Frames will *completely* be deleted in both new files.
