use lib "/home/CE/philipg/modules/XML-Twig-3.32/blib/lib/";
use XML::Twig;

my @files = </proj/llx/Semeval/fn1.4/corpusXML/*.xml>;

my $source_xml = new XML::Twig(twig_roots => {'corpus' => 1 }, keep_encoding => 1);

my $target1_xml = new XML::Twig(twig_roots => {'corpus' => 1 }, keep_encoding => 1, pretty_print => "indented");
my $target2_xml = new XML::Twig(twig_roots => {'corpus' => 1 }, keep_encoding => 1, pretty_print => "indented");

print("Parsing 0.xml\n");
$target1_xml -> parsefile("/proj/llx/Semeval/SalsaXml/fulltext/0.xml");
print("Parsing 1.xml\n");
$target2_xml -> parsefile("/proj/llx/Semeval/SalsaXml/fulltext/1.xml");

for my $file(@files){
	print("Parsing $file\n");
	$source_xml -> parsefile($file);
	for my $sentence($source_xml -> descendants("sentence")){
		$sent_ID = $sentence -> att("ID");
		@frames = $sentence -> descendants("annotationSet[\@frameName]");
		for($i = 0; $i <= $#frames; $i++){
			$frame = $frames[$i];
			@NI = ($frame -> descendants("label[\@itype=\"INI\"]"), $frame -> descendants("label[\@itype=\"DNI\"]"), $frame -> descendants("label[\@itype=\"CNI\"]"));
			if($#NI > -1){
				print("Correcting NI in s$sent_ID\n");
				$target_sent;
				@foo = $target1_xml -> descendants("s[\@id=\"$sent_ID\"]");
				if($#foo > -1){
					$target_sent = $foo[0];
				}else{
					$target_sent = ($target2_xml -> descendants("s[\@id=\"$sent_ID\"]"))[0];
				}
				$target_frame = ($target_sent -> descendants("frame"))[$i];
				for my $inst(@NI){
					$name = $inst -> att("name");
					$new_ID = $target_frame -> att("id") . "_fe" . $inst -> att("ID");
					$itype = $inst -> att("itype");
					$new_fe = XML::Twig::Elt->new("fe");
					$new_fe -> set_att(name => "$name", id => "$new_ID");
					$new_flag = XML::Twig::Elt -> new("flag");
					if($itype eq "INI"){
						$new_flag -> set_att(name => "Indefinite_Interpretation");
					}
					if($itype eq "DNI"){
						$new_flag -> set_att(name => "Definite_Interpretation");
					}
					if($itype eq "CNI"){
						$new_flag -> set_att(name => "Constructional_Interpretation");
					}
					$new_flag -> move(first_child, $new_fe);
					$new_fe -> move(last_child, $target_frame);
				}
			}
		}
	}
	$source_xml -> purge();
}

print("Writing 0.xml\n");
open(FILE, ">/proj/llx/Semeval/SalsaXml/fulltext/with_ni/0.xml") or die("Couldn't write new file");
$target1_xml -> print(\*FILE, "indented");
$target1_xml -> purge();
close(FILE);
print("Writing 1.xml\n");
open(FILE, ">/proj/llx/Semeval/SalsaXml/fulltext/with_ni/1.xml") or die("Couldn't write new file");
$target2_xml -> print(\*FILE, "indented");
$target2_xml -> purge();
close(FILE);
system('chown :llx /proj/llx/Semeval/SalsaXml/fulltext/with_ni/*.xml');
system('chmod ug+rw /proj/llx/Semeval/SalsaXml/fulltext/with_ni/*.xml');
