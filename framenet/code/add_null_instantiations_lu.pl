use lib "/home/CE/philipg/modules/XML-Twig-3.32/blib/lib/";
use XML::Twig;

my @files = </proj/llx/Semeval/fn1.4/luXML/*.xml>;

my $source_xml = new XML::Twig(twig_roots => {'lexunit-annotation' => 1 }, keep_encoding => 1);
my $target = new XML::Twig(twig_roots => {'corpus' => 1 }, keep_encoding => 1, pretty_print => 'indented');
my $cur_file = "/proj/llx/Semeval/SalsaXml/lu/with_ni/69.xml";
print("Opening $cur_file\n");
$target -> parsefile($cur_file);

for my $file(@files){
	print("Parsing $file\n");
	$source_xml -> parsefile($file);
	$source_ID = ($source_xml -> root) -> att("ID");
	for my $annotation_set($source_xml -> descendants("annotationSet")){
		$sentence = ($annotation_set -> descendants("sentence"))[0];
		$sent_ID = $sentence -> att("ID");
		$real_ID = $source_ID . "-" . $sent_ID;

		@frames = $annotation_set -> descendants("layer[\@name=\"FE\"]");
		for($i = 0; $i <= $#frames; $i++){
			$frame = $frames[$i];
			@NI = ($frame -> descendants("label[\@itype=\"INI\"]"), $frame -> descendants("label[\@itype=\"DNI\"]"), $frame -> descendants("label[\@itype=\"CNI\"]"));
			if($#NI > -1){
				print("Correcting NI in s$sent_ID\n");
				@foo = $target -> descendants("s[\@id=\"$real_ID\"]");
				if($#foo < 0){
					print("Changing target\n");
			                $grep_file = (split(/:/, `grep "<s " /proj/llx/Semeval/SalsaXml/lu/with_ni/*.xml | grep "$real_ID"`))[0];
					print("Writing back $cur_file\n");
	                                open(FILE, ">$cur_file");
					$target -> print(\*FILE, "indented");
					$target -> purge();
	        	                close(FILE);
					print("Re-Opening\n");
					$target -> parsefile($cur_file);
					$target -> purge();
					print("Opening $grep_file\n");
               			        $target -> parsefile($grep_file);
	        	                $cur_file = $grep_file;
				}
				$target_sent = ($target -> descendants("s[\@id=\"$real_ID\"]"))[0];
				$target_frame = ($target_sent -> descendants("frame"))[$i];
				for my $inst(@NI){
					$name = $inst -> att("name");
					$new_ID = $target_frame -> att("id") . "_fe" . $inst -> att("ID");
					$itype = $inst -> att("itype");
					$new_fe = XML::Twig::Elt->new("fe");
					$new_fe -> set_att(name => "$name", $id => "$new_ID");
					$new_flag = XML::Twig::Elt -> new("flag");
					if($itype eq "INI"){
						$new_flag -> set_att(name => "Indefinite_Interpretation");
					}
					if($itype eq "DNI"){
						$new_flag -> set_att(name => "Definite_Interpretation");
					}
					if($itype eq "CNI"){
						$new_flag -> set_att(name => "Constructional_Interpretation");
					}
					$new_flag -> move(first_child, $new_fe);
					$new_fe -> move(last_child, $target_frame);
				}
			}
		}
	}
	$source_xml -> purge();
}

print("Writing back $cur_file\n");
open(FILE, ">$cur_file");
$target -> print(\*FILE, "indented");
close(FILE);
$target -> purge();

system('chown :llx /proj/llx/Semeval/SalsaXml/lu/with_ni/*.xml');
system('chmod ug+rw /proj/llx/Semeval/SalsaXml/lu/with_ni/*.xml');
