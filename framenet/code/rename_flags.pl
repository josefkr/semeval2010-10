#!/usr/bin/perl
use lib "/home/CE/philipg/modules/XML-Twig-3.32/blib/lib";
use XML::Twig;
use Getopt::Long;

GetOptions(
	"file=s" => \$file,
);

print("Reading $file\n");

my $twig = new XML::Twig(twig_roots => {'corpus' => 1 }, keep_encoding => 1, pretty_print => "indented");
$twig -> parsefile($file);

print("Changing INI to Indefinite_Interpretation\n");
for my $flag($twig -> descendants("flag[\@name=\"INI\"]")){
	$flag -> set_att(name => "Indefinite_Interpretation");
}

print("Changing DNI to Definite_Interpretation\n");
for my $flag($twig -> descendants("flag[\@name=\"DNI\"]")){
	$flag -> set_att(name => "Definite_Interpretation");
}

print("Changing CNI to Constructional_Interpretation\n");
for my $flag($twig -> descendants("flag[\@name=\"CNI\"]")){
	$flag -> set_att(name => "Constructional_Interpretation");
}

print("Writing back $file\n");
open(FILE, ">$file");
$twig -> print(\*FILE, "indented");
$twig -> purge();
close(FILE);
