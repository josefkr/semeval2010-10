#!/usr/bin/perl
use lib "/home/CE/philipg/modules/XML-Twig-3.32/blib/lib";
use XML::Twig;
use Getopt::Long;

GetOptions(
	"file=s" => \$file,
);

print("Reading $file\n");

my $twig = new XML::Twig(twig_roots => {'corpus' => 1 }, keep_encoding => 1, pretty_print => "indented");
$twig -> parsefile($file);

print("Removing unseen frames\n");
for my $frame($twig -> descendants("frame")){
	@unseen = $frame -> descendants("flag[\@name=\"Unseen\"]");
	if($#unseen > -1){
		$frame -> delete;
	}
}

print("Writing back $file".".no_unseen.xml\n");
open(FILE, ">$file".".no_unseen.xml");
$twig -> print(\*FILE, "indented");
$twig -> purge();
close(FILE);
system("chown :llx $file".".no_unseen.xml");
system("chmod g+rw $file".".no_unseen.xml");
