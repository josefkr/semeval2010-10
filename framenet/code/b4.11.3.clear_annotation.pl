use lib "/home/CE/philipg/modules/XML-Twig-3.32/blib/lib/";
use XML::Twig;

my $file = $ARGV[0];

print("\nParsing " . $file . "\n");
$twig = new XML::Twig(twig_roots => {'corpus' => 1 }, keep_encoding => 1);
$twig -> parsefile($file);

print("Done!\n\nRemoving Coreference-Frames\n");
$root = $twig -> root();
for my $coref($root -> descendants("frame[\@name=\"Coreference\"]")){
	$coref -> delete;
}

print("Done!\n\nCreating Fulltask-File\n");
$fulltask = $root -> copy();
for my $fe($fulltask -> descendants("fe")){
	$fe -> delete;
}
for my $flag($fulltask -> descendants("flag")){
	$flag -> delete;
}
open(FILE, ">".$file."_fulltask.xml");
$fulltask -> print(\*FILE, "indented");
close(FILE);
system('chown :llx "' . $file . '_fulltask.xml"');
system('chmod ug+rw "' . $file . '_fulltask.xml"');

print("Done!\n\nCreating No-NullInstantiation-File\n");
$ni = $root -> copy();
for my $fe($ni -> descendants("fe")){
	@dni = $fe -> descendants("flag[\@name=\"Definite_Interpretation\"]");
	@ini = $fe -> descendants("flag[\@name=\"Indefinite_Interpretation\"]");
	# CS: added following lines to catch NIs without 
        # "Definite_Interpretation" or "Indefinite_Interpretation" flag 
	# (e.g. s394_f1_e1 in TigerOfSanPedro.withHeads.xml)
	@constr_lic_ni = $fe -> descendants("flag[\@name=\"Constructional_licensor\"]");
	@lex_lic_ni = $fe -> descendants("flag[\@name=\"Lexical_licensor\"]");
	@uncert_lic_ni = $fe -> descendants("flag[\@name=\"Uncertain_Licensor\"]");
		    
	if(($#dni > -1) || ($#ini > -1) || ($#constr_lic_ni > -1) || ($#lex_lic_ni > -1) || ($#uncert_lic_ni > -1)){
		$fe -> delete;
	}
}
open(FILE, ">".$file."_no-nullinstantiation.xml");
$ni -> print(\*FILE, "indented");
close(FILE);
system('chown :llx "' . $file . '_no-nullinstantiation.xml"');
system('chmod ug+rw "' . $file . '_no-nullinstantiation.xml"');
print("All done!\n");

