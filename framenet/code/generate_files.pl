use Getopt::Long;

GetOptions(
	"file=s" => \$file,
	"headFile=s" => \$headFile
);
$withHeads = $file . ".heads.xml";

print("Adding heads\n");
system("java -classpath /proj/llx/Semeval/Java/semeval/bin semeval.headMerge.HeadMerger annoFile=\"$file\" headFile=\"$headFile\" destination=\"$withHeads\"");
system("chown :llx $withHeads");
system("chmod g+rw $withHeads");
print("Correcting flags\n");
system("perl /proj/llx/Semeval/scripts/rename_flags.pl --file=\"$withHeads\"");
print("Creating files\n");
system("perl /proj/llx/Semeval/scripts/clear_annotation.pl $withHeads");
